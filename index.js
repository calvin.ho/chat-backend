require("dotenv").config();
const express = require("express");
const socketio = require("socket.io");
const http = require("http");
const router = require("./router");
const moment = require("moment");
const POST = process.env.POST || 5000;
const app = express();
const server = http.createServer(app);
const io = socketio(server);
const axios = require("axios");
// const util = require('minecraft-server-util');
const cors = require("cors");
const jwt = require("jsonwebtoken");

const {
  addUser,
  removeUser,
  getUser,
  getUsersInRoom,
  storeMessages,
} = require("./users.js");
const newsUrl =
  "http://newsapi.org/v2/top-headlines?country=hk&category=general&apiKey=1d9bc6061e3e459fa583927c62e914e7";
const History = require("./src/models/chat.history.model");
const Room = require("./src/models/chat.room.model");
const User = require("./src/models/user.model");
const bcrypt = require("bcrypt");

app.use(express.json());
app.use(cors());

app.get("/news", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  ); // If needed
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  ); // If needed
  res.setHeader("Access-Control-Allow-Credentials", true); // If needed
  axios
    .get(newsUrl)
    .then((response) => {
      res.send(response.data);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
});

app.get("/getRoom", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  ); // If needed
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  ); // If needed
  res.setHeader("Access-Control-Allow-Credentials", true); // If needed
  Room.getRoomLists((err, data) => {
    if (err) {
      res.status(500).send(err);
    }
    res.send(data);
  });
});

app.post("/createUser", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  ); // If needed
  res.header("Access-Control-Allow-Headers", "*");
  res.setHeader("Access-Control-Allow-Credentials", true); // If needed
  const { username, password } = req.body;
  if (username && password) {
    const insertValues = {
      username: username,
      password: bcrypt.hashSync(password, 10), // 密碼加密
      date: moment(new Date()).unix(),
    };

    User.createUserAccount(insertValues, (data) => {
      if (!data.status) {
        res.send(500, data);
      } else {
        res.send(200, data);
      }
    });
  } else {
    res.send(500, `nullpointexception`);
  }
});

app.post("/login", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  ); // If needed
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  ); // If needed
  res.setHeader("Access-Control-Allow-Credentials", true); // If needed
  const { username, password } = req.body;
  if (username && password) {
    const insertValues = {
      username: username,
    };

    User.selectUserInfo(insertValues, (data) => {
      if (!data.status) {
        res.send(500, data);
      } else {
        const [user] = JSON.parse(JSON.stringify(data.data));
        bcrypt.compare(password, user.password, (err, result) => {
          // 產生 JWT
          if (result) {
            const payload = {
              username: user.username,
            };
            const token = jwt.sign(
              { payload, exp: Math.floor(Date.now() / 1000) + 60 * 15 },
              "key"
            );
            res.send(200, {
              loginStatus: true,
              message: "login success",
              token: token,
            });
          } else {
            res.send(200, {
              loginStatus: false,
              message: "login failed",
            });
          }
        });
      }
    });
  } else {
    res.send(500, `nullpointexception`);
  }
});

// app.get('/mc', (req, res) => {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
//     res.setHeader('Access-Control-Allow-Credentials', true); // If needed
//     const client = new util.RCON('34.84.136.71', { port: 25575, password: 'minecraft' });
//     client.on('output', (message) => console.log(message));

//     client.connect()
//         .then(async () => {
//             console.log("connect:");
//             await client.run('list').then(res=> {
//                 console.log(res);
//             }); // List all players online
//             client.close();
//         })
//         .catch((error) => {
//             throw error;
//             res.send(500);
//         });

//     res.send(testData);
// });

io.on("connect", (socket) => {
  const { payload } = jwt.verify(socket.handshake.query["token"], "key");
  socket.emit("conn", { username: payload.username });

  socket.on("join", ({ name, room }, callback) => {
    console.log(name + " online");
    const { error, user } = addUser({ id: socket.id, name, room });

    if (error) return callback(error);

    socket.join(user.room);
    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room),
    });

    const info = new History({
      roomId: user.room,
    });

    History.getHistoryAll(info, (err, data) => {
      if (err) {
        return callback(err);
      }
      io.to(user.room).emit("historys", JSON.parse(JSON.stringify(data)));
    });
    socket.emit("message", {
      username: "",
      message: `${user.name}, welcome to ${user.room}.`,
    });
    socket.broadcast
      .to(user.room)
      .emit("message", { username: "", message: `${user.name} has joined!` });
    callback();
  });

  socket.on("sendMessage", (message, callback) => {
    const user = getUser(socket.id);
    if (!user) {
      return socket.emit("errMsg", false);
    } else {
      const now = moment(new Date()).unix();
      storeMessages(
        { user: user.name, text: message, datetime: now },
        user.room
      );
      socket.emit("errMsg", true);
    }
    io.to(user.room).emit("message", { username: user.name, message: message });
    io.to(user.room).emit("roomData", {
      username: user.room,
      message: getUsersInRoom(user.room),
    });
    callback();
  });

  socket.on("disconnect", () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit("message", {
        username: "admin",
        message: `${user.name} has left.`,
      });
    }
  });
});

app.use(router);
server.listen(POST, () => console.log(`Server has started on post ${POST}`));
