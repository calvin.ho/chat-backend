const sql = require("./db.js");

// constructor
const History = function(info) {
    this.username = info.username;
    this.message = info.message;
    this.date = info.date;  
    this.roomId = info.roomId;
};

History.storeMessage = (data, result)  => {
    sql.query(`INSERT INTO chat_history (username, message, created_at, room_id)
    VALUES  ("${data.username}","${data.message}",${data.date},${data.roomId})`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            return result(null, err);
        }
    });
};

History.getHistoryAll = (data, result)  => {
    sql.query(`SELECT * FROM chat_history where room_id = ${data.roomId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            return result(null, err);
        }
        return result(null, res);
    });
};

module.exports = History;