const sql = require("./db.js");

// constructor
const User = function(data) {
  this.username = data.username;
  this.password = data.password;
  this.date = data.date;
};

User.createUserAccount = (data, result) => {
    sql.query(`INSERT INTO chat_user (username, password,created_at) VALUES ("${data.username}","${data.password}",${data.date})`, (err, res) => {
        if (err) {
            console.log("error: ", err.code);
            return result({
                'status': false,
                'data': err.code
            });
        }
        return result({
            'status': true,
            'data': 'success'
        });
    });
};

User.selectUserInfo = (data, result) => {
    sql.query(`SELECT username, password from chat_user where username = "${data.username}"`, (err, res) => {
        if (err) {
            console.log("error: ", err.code);
            return result({
                'status': false,
                'data': err.code
            });
        }
        if(res.length > 0) {
            return result({
                'status': true,
                'data': res
            });
        }else {
            return result({
                'status': false,
                'data': 'user not found'
            });
        }
    });
};


module.exports = User;
