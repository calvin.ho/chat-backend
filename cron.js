const _ = require('lodash');

const sendWhatsappMessage = (accountSid, authToken, datas) => {
  const client = require('twilio')(accountSid, authToken); 
  if(_.size(datas) <= 0) {
    client.messages 
      .create({ 
          body: datas, 
          from: 'whatsapp:+14155238886',  //todo: keep personal data to env      
          to: 'whatsapp:+85297721908' 
        }) 
      .done();
  }else {
    let sendwhatsappMsg = '';
    _.forEach(datas, function(value, key) {
      // const url = (value.url).length <= 50 ? value.url: 'url too long';  //todo: use shortUrl ,solve it
      // const oneNew = value.title + '\n' + url;
      const oneNew = value.title;
      sendwhatsappMsg += oneNew + '\n\n';
    });
     client.messages 
      .create({ 
          body: sendwhatsappMsg, 
          from: 'whatsapp:+14155238886',       
          to: 'whatsapp:+85297721908' 
        })  
      .done();
  } 
}

module.exports = { sendWhatsappMessage };