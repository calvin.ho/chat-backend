require("dotenv").config();
const History = require("./src/models/chat.history.model");
let users = [];
const addUser = ({ id, name, room }) => {
  name = name.trim().toLowerCase();
  room = room;

  //users = users.filter(function (user) {
    //return user.room !== room && user.name !== name;
  //});
  // users.filter((user) => user.room === room && user.name === name);
  const existingUser = users.find(
    (user) => user.name === name
  );

  if (!name || !room) return { error: "Username and room are required." };
  if (existingUser) return { error: "Username is taken." };

  const user = { id, name, room };
  users.push(user);
  return { user };
};

const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) return users.splice(index, 1)[0];
};

const getUser = (id) => users.find((user) => user.id === id);

const getUsersInRoom = (room) => users.filter((user) => user.room === room);

const storeMessages = (message, room) => {
  const info = new History({
    roomId: room,
    username: message.user,
    message: message.text,
    date: message.datetime,
  });

  History.storeMessage(info, (err, data) => {
    if (err) {
      return callback(err);
    }
  });
  // const obj = JSON.parse(fs.readFileSync(process.env.CHATHISTORYPATH, 'utf8'));
  // if(obj) {
  //   obj[room].push(message);
  //   fs.writeFileSync(process.env.CHATHISTORYPATH, JSON.stringify(obj), (err) => {
  //       if (err) throw err;
  //   });
  // }
};

module.exports = {
  addUser,
  removeUser,
  getUser,
  getUsersInRoom,
  storeMessages,
};
